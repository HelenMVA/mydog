import { CdkAriaLive } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-modal-add-adopt-announce-admin',
  templateUrl: './modal-add-adopt-announce-admin.component.html',
  styleUrls: ['./modal-add-adopt-announce-admin.component.scss'],
})
export class ModalAddAdoptAnnounceAdminComponent implements OnInit {
  FormAdoptAnnoncee: FormGroup = this.fb.group({
    img: [''],
  });
  selectedFile: File = null;
  fd = new FormData();

  constructor(private fb: FormBuilder, private adminService: AdminService) {}

  ngOnInit(): void {}
  createFormData(e) {
    console.dir(e.target.files.length);
    console.log(this.FormAdoptAnnoncee);
    if (e.target.files.length > 0) {
      console.dir(e);
      const file = e.target.files[0];
      console.log(file);
      this.FormAdoptAnnoncee.get('img').setValue(file);
    }
  }
  sendNewAnnonce() {
    const formData = new FormData();
    formData.append('file', this.FormAdoptAnnoncee.get('img').value);
    console.log(formData);
    this.adminService.sendUploadImg(formData).subscribe(
      (data) => {
        console.dir('ok');
      },
      (err) => {
        console.dir(err);
      }
    );
  }
}
