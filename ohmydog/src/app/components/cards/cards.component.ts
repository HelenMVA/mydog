import { Component, OnInit } from '@angular/core';
import { AdoptService } from 'src/app/services/adopt.service';

interface Sex {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
  allDogs:any[] = [];
  filteredDogs:any[] = [];
  data: any;
  filterDogs :any[] = [];
  cardLoiding: boolean;

  dogSex: Sex[] = [
    {value: 'tous', viewValue: 'Tous'},
    {value: '1', viewValue: 'Male'},
    {value: '2', viewValue: 'Femelle'},
  ];
  constructor(private adoptionService: AdoptService) { }

  ngOnInit(): void {
    window.scroll(0,0);
    this.getAdoptionDogs();
  }
  getAdoptionDogs() {
    this.cardLoiding = true;
    this.adoptionService.getAdoptionDogs().subscribe((res: any) => {
     this.allDogs = res.message;
     this.filterDogs = res.message;
     this.cardLoiding = false;
    });
  }
  /**
   * Filter
   */
  filterSex(ev){
if(ev.value.value === '1' || ev.value.value === '2'){
  this.filterDogs = this.allDogs.filter(dog => dog.sexe == ev.value.value);
} else {
  this.filterDogs = this.allDogs;
}
  }
}
