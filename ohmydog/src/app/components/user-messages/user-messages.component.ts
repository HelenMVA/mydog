import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-messages',
  templateUrl: './user-messages.component.html',
  styleUrls: ['./user-messages.component.scss'],
})
export class UserMessagesComponent implements OnInit, OnDestroy {
  FormMessage: FormGroup = this.fb.group({
    message: ['', Validators.required],
  });

  currentUserSubscription: Subscription;
  currentUser;
  userId: string;
  userMessages: [];
  // myUsers: Array<any> = [];
  myUsers: [] = [];

  from_user_id: string;
  constructor(private userService: UserService,
    private currentUserService: CurrentUserService,
    private fb: FormBuilder) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user: any) => {
        this.currentUser = user;
        this.userId = user.data.user.id;
        // this.getUserMessage(this.userId);
        this.getMyUsers(this.userId);
      }
    );
  }

  ngOnInit(): void { }
  // getUserMessage(user_id) {
  //   return this.userService.getUserMessage(user_id).subscribe((res: any) => {
  //     this.userMessages = res.data;
  //     // console.dir(this.userMessages);
  //     this.goMyUsers();
  //   });
  // }
  getMyUsers(user_id) {
    return this.userService.getMyUsers(user_id).subscribe((res: any) => {
      this.myUsers = res.data;
      this.myUsers.forEach((myUser: any) =>
        this.getUserMessage(myUser.id))
    });
  }

  getUserMessage(from_user_id) {
    this.from_user_id = from_user_id;
    return this.userService
      .getUserMessage(from_user_id, this.userId)
      .subscribe((res: any) => {
        this.userMessages = res.data;
      });
  }
  sendMessage() {
    // console.dir(this.from_user_id);
    const from_user_id = this.userId;
    const to_user_id = this.from_user_id;
    const message = this.FormMessage.value.message;
    const sendMessage = {
      from_user_id,
      to_user_id,
      message,
    };
    this.userService.sendMessage(sendMessage).subscribe(
      (data) => {
        console.dir('mess send');
        this.FormMessage.reset();
        this.getUserMessage(this.from_user_id);
      },
      (err) => {
        console.dir(err);
      }
    );
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
