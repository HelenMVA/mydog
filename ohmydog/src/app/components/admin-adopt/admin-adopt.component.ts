import { Component, OnInit, Sanitizer } from '@angular/core'
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from 'src/app/services/admin.service';
import { ModalAddAdoptAnnounceAdminComponent } from '../modal-add-adopt-announce-admin/modal-add-adopt-announce-admin.component';
import { ModalInfoComponent } from '../modal-info/modal-info.component';
import { ModalSendMessageComponent } from '../modal-send-message/modal-send-message.component';
import { AdoptDemand } from '../../Models/adopt-demand';


@Component({
  selector: 'app-admin-adopt',
  templateUrl: './admin-adopt.component.html',
  styleUrls: ['./admin-adopt.component.scss'],
})
export class AdminAdoptComponent implements OnInit {
  allDemandAdoption: any[] = [];
  imageToShow: any;
  test: any;
  url: any;

  constructor(
    private modalService: NgbModal,
    private adminService: AdminService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.getAllDemandes();
  }
  getAllDemandes() {
    this.adminService.getAllDemandes().subscribe((res: any) => {
      this.allDemandAdoption = res.message;
    });
  }
  /**
   * 
   * On accepte le demande user pour adopter un chien
   * ensuite on appele function pour mette non valaible de cette chien dans la table adopt chien
   * ensuite on refuse les autre demandes pour cette chiens
   */
  acceptDemand(demand: AdoptDemand) {
    this.adminService.accept(demand.id_demand).subscribe(() => {
      demand.status = 'accept';
      this.changeAvailableDog(demand.id_dog);
      this.allDemandAdoption.forEach(el => {
        if (el.id_demand != demand.id_demand && el.id_dog === demand.id_dog) {
          this.rejectDemand(el);
          // el.status = 'reject'
        }
      })
    });
    // this.getAllDemandes();
  }
  changeAvailableDog(dog_id) {
    const dog_avalaible = {
      avalaible: 0
    }
    this.adminService.changeAvailableDog(dog_id, dog_avalaible).subscribe(res => console.dir(res));
  }


  rejectDemand(demand: AdoptDemand) {
    this.adminService.reject(demand.id_demand).subscribe(() => {
      // this.toastService.success("Demande a été rejeté ");
      // this.getAllDemandes();
      demand.status = 'reject'
    });
  }

  sendMessage(demand) {
    const modalRef = this.modalService.open(ModalSendMessageComponent);
    modalRef.componentInstance.userReceiverId = demand.id;
    // modalRef.result.then((result) => {
    //   console.log(result);
    // });
  }
  openModalInfo(demand, parametr) {
    const modalRef = this.modalService.open(ModalInfoComponent);
    modalRef.componentInstance.info = demand;
    modalRef.componentInstance.param = parametr;
  }

  openModalAddAnnounce() {
    this.modalService.open(ModalAddAdoptAnnounceAdminComponent);
  }
}
