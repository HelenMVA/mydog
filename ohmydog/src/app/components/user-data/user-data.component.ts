import { NullTemplateVisitor } from '@angular/compiler';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { rejects } from 'assert';
import { resolve } from 'dns';
import { promise } from 'protractor';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UploadImgService } from 'src/app/services/upload-img.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent implements OnInit, OnDestroy {
  selectedFiles: FileList;
  file;
  FOLDER = 'furry_friends/';
  filekey: String;
  fileUrl = 'https://alyona.s3.eu-west-3.amazonaws.com/';
  url: string;

  currentUserSubscription: Subscription;
  currentUser;
  userId: string;
  constructor(private userService: UserService, private currentUserService: CurrentUserService,
    private imageUploadService: UploadImgService) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user: any) => {
        console.dir(user);
        this.currentUser = user;
        this.userId = user.data.user.id;
      }
    );
  }

  ngOnInit(): void {
    window.scroll(0, 0);
  }

  onSubmit(user) {
    console.dir(user.form.value);
  }

  editAvatar() {
    console.dir('delete');
  }
  deleteAvatar() {
    this.userService.deleteAvatar(this.currentUser.data.user.id, null).subscribe((resp) => {
      this.currentUser.data.user.avatar_url = null;
      localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
    });
  }

  selectFile(event) {
    return new Promise((resolve, reject) => {
      this.selectedFiles = event.target.files;
      this.file = this.selectedFiles.item(0);
      this.filekey = this.FOLDER + (Date.now() + this.selectedFiles.item(0).name);
      console.dir(this.filekey);
      this.imageUploadService.uploadFile(this.file, this.filekey);
      this.url = this.fileUrl + this.filekey;
      console.dir(this.url);
      const avatar_url = (this.fileUrl + this.filekey);
      const url = {
        avatar_url
      };
      this.userService.updateAvatar(this.currentUser.data.user.id, url).subscribe((res: any) => {
        this.currentUser.data.user.avatar_url = res.message;
        this.currentUserService.setCurrenUser(this.currentUser);
        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
        console.dir(this.currentUser);
      })
    }).then(() => (
      this.userService.updateAvatar(this.currentUser.data.user.id, this.url).subscribe(res => {
        // this.currentUser.data.user.avatar_url = (this.file + this.filekey)
        console.dir(res);
      })
    ));
  }


  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
