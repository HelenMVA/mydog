import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';

@Component({
  selector: 'app-main-home-page',
  templateUrl: './main-home-page.component.html',
  styleUrls: ['./main-home-page.component.scss'],
})
export class MainHomePageComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    AOS.init();
  }
}
