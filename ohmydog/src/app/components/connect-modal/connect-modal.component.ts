import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/Models/user';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-connect-modal',
  templateUrl: './connect-modal.component.html',
  styleUrls: ['./connect-modal.component.scss']
})
export class ConnectModalComponent implements OnInit {
  currentUser;

  FormConnect: FormGroup = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });


  constructor(private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private userService: UserService,
    private toastService: ToastrService) { }

  ngOnInit(): void {
  }
  send() {
    this.userService.connect(this.FormConnect.value).subscribe(data => {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.closeModal(this.currentUser);
    }, (err) => {
      this.toastService.error(err);
    }
    );
  }
  closeModal(param) {
    this.activeModal.close(param);
  }


}
