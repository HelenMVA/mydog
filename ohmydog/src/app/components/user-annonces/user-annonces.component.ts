import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { KeepDog } from 'src/app/Models/keepDog';
import { User } from 'src/app/Models/user';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-annonces',
  templateUrl: './user-annonces.component.html',
  styleUrls: ['./user-annonces.component.scss']
})
export class UserAnnoncesComponent implements OnInit, OnDestroy {
  currentUserSubscription: Subscription;
  currentUser: User;
  userId: string;
  cardLoiding: boolean;
  myAnnonces: KeepDog[] = [];

  constructor(private userService: UserService,
    private curretUserService: CurrentUserService,) {
    this.currentUserSubscription = this.curretUserService.currentUser.subscribe(
      (user: any) => {
        this.currentUser = user;
        this.userId = user.data.user.id;
        this.getUsernnonces(this.userId);
      }
    );
  }

  ngOnInit(): void {
    window.scroll(0, 0);
  }
  getUsernnonces(owner_id) {
    this.cardLoiding = true;
    return this.userService.getUsernnonces(owner_id).subscribe((res: any) => {
      console.dir(res);
      this.myAnnonces = res.data;
      this.cardLoiding = false;
    });
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
