import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.component.html',
  styleUrls: ['./modal-info.component.scss'],
})
export class ModalInfoComponent implements OnInit {
  @Input() public param;
  @Input() public info;

  constructor() {}

  ngOnInit(): void {
    console.dir(this.param);
    console.dir(this.info);
  }
}
