import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ModalSendMessageComponent } from 'src/app/components/modal-send-message/modal-send-message.component';
import { ModaleGardeUserAnnonceComponent } from 'src/app/components/modale-garde-user-annonce/modale-garde-user-annonce.component';
import { ConfirmModalComponent } from 'src/app/components/modals/confirm-modal/confirm-modal.component';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { KeepDogService } from 'src/app/services/keep-dog.service';
import { UploadImgService } from 'src/app/services/upload-img.service';
import { UserService } from 'src/app/services/user.service';
import { KeepDog } from '../../Models/keepDog'

@Component({
  selector: 'app-garde-page',
  templateUrl: './garde-page.component.html',
  styleUrls: ['./garde-page.component.scss']
})
export class GardePageComponent implements OnInit, OnDestroy {
  keepDog: KeepDog;
  currentUserSubscription: Subscription;
  currentUser;
  cardLoiding: boolean;
  allDogs: any[] = [];
  filtredDogs: any[] = [];
  startDate: Date;
  endDate: Date;
  constructor(
    private modalService: NgbModal,
    private userService: UserService,
    private router: Router,
    private toastService: ToastrService,
    private currentUserService: CurrentUserService,
    private keepService: KeepDogService) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user) => {
        this.currentUser = user;
      }
    );
  }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.getKeepDogs();
  }

  showAddAnnonceModal() {
    const modalRef = this.modalService.open(ModaleGardeUserAnnonceComponent);
    modalRef.componentInstance.currentUser = this.currentUser;
    modalRef.result.then((result) => {
      this.allDogs.push(result.message);
    });
  }
  /**
   * On recuper all annonces Keep dog pour afficher 
   * sur la page
   */
  getKeepDogs() {
    this.cardLoiding = true;
    this.keepService.getKeepDogs().subscribe((res: any) => {
      this.allDogs = res.message;
      this.filtredDogs = res.message;
      this.cardLoiding = false;
      this.getUserKeepDods()
    });
  }
  /**
   * on recuper les demandes Keed dog
   * de current user
   * pour ne pas afficher button "demand"
   * si user l'a deja
   */
  getUserKeepDods() {
    this.userService.getUserKeepDogs(this.currentUser.data.user.id).subscribe((res: any) => {
      res.data.forEach(element => {
        this.allDogs.forEach((dog: KeepDog) => {
          if (element.dog_id === dog.dog_id) {
            dog.sendDemand = true;
          }
        })
      });
    })
  }

  // filter DATE
  dateStart(e) {
    this.startDate = e.value;
  }
  dateEnd(e) {
    this.endDate = e.value;
    if (this.startDate && this.endDate) {
      this.filtredDogs = this.allDogs.filter(dog => ((new Date(this.startDate).getTime()) < (new Date(dog.date_start).getTime()) && (new Date(this.endDate).getTime()) > (new Date(dog.date_fin).getTime()))
      );
    } else {
      this.filtredDogs = this.allDogs;
    }
  }


  /**
   * Show modal Send Message 
   */

  showSendMessageModal(dog: KeepDog) {
    const modalRef = this.modalService.open(ModalSendMessageComponent);
    modalRef.componentInstance.userReceiverId = dog.owner_id;
    modalRef.result.then((result) => {
      if (result && result != null) {
        this.toastService.success("votre message etai envoyé");
      }
    });
  }
  /**
     * Show modal Send Demand 
     */

  sendDemand(dog: KeepDog) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.text = "Voulez vous faire le demande pour garder ce chien?";
    modalRef.result.then((result) => {
      if (result && result === 'yes') {
        const body = {
          dog_id: dog.dog_id,
          user_id: this.currentUser.data.user.id,
          owner_id: dog.owner_id
        }
        this.keepService.sendDemand(body).subscribe(res => {
          this.toastService.success('votre demand etait envoye')
          dog.sendDemand = true;
        })
      }
    });
  }

  deleteMyAnnonce(dog) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.text = "Voulez vous supprimer votre annonce?";
    modalRef.result.then((result) => {
      if (result && result === 'yes') {
        const body = {
          dog_id: dog.dog_id,
        }
        this.keepService.deleteMyAnnonce(body).subscribe(res => {
          this.toastService.success('votre annonce etait supprime')
          console.dir(res);
        })
      }
    });
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
