import {NgModule} from '@angular/core';
import {PipeModule} from '../../pipes/pipe.module';

@NgModule({
    imports: [
        PipeModule
    ],
    declarations: [
        
    ],
    providers: [
        
    ]
})
export class GardePageModule {
}