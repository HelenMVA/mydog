import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-my-profil',
  templateUrl: './my-profil.component.html',
  styleUrls: ['./my-profil.component.scss'],
})
export class MyProfilComponent implements OnInit, OnDestroy {
  currentUserSubscription: Subscription;
  currentUser;
  userId: string;

  constructor(private userService: UserService,
    private router: Router,
    private currentUserService: CurrentUserService) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user: any) => {
        if (user != null) {
          console.dir(user);
          this.currentUser = user;
          this.userId = user.data.user.id;
        } else {
          this.router.navigate(['/']);
        }

      }
    );
  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
