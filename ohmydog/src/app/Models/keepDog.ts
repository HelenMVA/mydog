export interface KeepDog {
    dog_id: string;

    name: string;

    owner_id: string;

    age: string;

    photo: string;

    characteristic: string;

    sexe: string;

    race: string;

    slug: string;

    date_start: Date;

    date_fin: Date;

    address: string;

    love_dog: boolean;

    love_cat: boolean;

    love_child: boolean;

    classification: string;
    sendDemand: boolean;
}
