import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../Models/user';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { CurrentUserService } from './current-user.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  apiURL = 'http://localhost:8000/back/';

  constructor(private http: HttpClient) {
  }

  register(user: User) {
    return this.http.post(`${this.apiURL}users/register`, user);
  }

  connect(connectUser: User) {
    return this.http.post(`${this.apiURL}users/authenticate`, connectUser).pipe(
      tap((user) => this.doLogin(user)),
      mapTo(true),
      catchError((err) => err)
    );
  }
  /**
   *
   * On save to localStorage
   */
  doLogin(user: any) {
    localStorage.setItem('currentUser', JSON.stringify(user));
  }
  logOut() {
    localStorage.removeItem('currentUser');
  }

  /**
   *
   * On recupere all Adopt dogs  demandes by this user
   */

  getUserDods(id_user) {
    return this.http.get(`${this.apiURL}users/${id_user}`);
  }
  /**
  *
  * On recupere all Keep dogs  demandes by this user
  */

  getUserKeepDogs(id_user) {
    return this.http.get(`${this.apiURL}users/keepDog/${id_user}`);
  }
  /**
   *
   * send message admin => user
   */

  sendMessage(message) {
    return this.http.post(`${this.apiURL}users/message`, message);
  }

  getUserMessage(id_user, me_id) {
    return this.http.get(`${this.apiURL}users/messages/${me_id}/${id_user}`);
  }
  getMyUsers(id_user) {
    return this.http.get(`${this.apiURL}users/myusers/${id_user}`);
  }
  rejectMyAdoptDemand(id) {
    return this.http.put(`${this.apiURL}users/rejectMyAdoptDemand/${id}`, 'user-cancel');
  }
  getUsernnonces(owner_id) {
    return this.http.get(`${this.apiURL}users/myannonces/${owner_id}`);
  }
  deleteAvatar(userId, avatar) {
    return this.http.put(`${this.apiURL}users/deleteAvatar/${userId}`, avatar);
  }

  updateAvatar(userId, url) {
    console.log(url);
    console.dir('service');
    return this.http.put(`${this.apiURL}users/editAvatar/${userId}`, url);
  }
}
