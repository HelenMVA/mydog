import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KeepDogService {
  apiURL = 'http://localhost:8000/back/keep/';

  constructor(private http: HttpClient) { }

  addAnnonce(annonce) {
    return this.http.post(`${this.apiURL}annonce`, annonce);
  }
  getKeepDogs() {
    return this.http.get(`${this.apiURL}dogs`);
  }

  sendDemand(demand) {
    return this.http.post(`${this.apiURL}keepDog`, demand);
  }
  deleteMyAnnonce(body) {
    return this.http.put(`${this.apiURL}deleteAnnonce/${body.dog_id}`, body);
  }
}
