import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/Models/user';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserGuard implements CanActivate {
  currentUser: User;

  constructor(private router: Router) {
  }

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser) return true;
      else  this.router.navigate(['/']);
  }
  
}
