import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment"
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';


@Injectable({
  providedIn: 'root'
})
export class UploadImgService {
  apiURL = 'http://localhost:8000/back/';
  FOLDER = 'furry_friends/';
  myUrl: String;


  constructor(private http: HttpClient) { }
  // UPloaad img bew garde dog annonce
  uploadFile(file, key) {
    console.dir('img')
    const contentType = file.type;
    const bucket = new S3(
      {
        accessKeyId: environment.ACCESS_KEY,
        secretAccessKey: environment.SECRET_KEY,
        region: 'eu-west-3'
      }
    );
    const params = {
      Bucket: 'alyona',
      // Key: this.FOLDER +  (Date.now() + file.name),
      Key: key,
      Body: file,
      ACL: 'public-read',
      ContentType: contentType
    };
    return bucket.upload(params, function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        return err;
      }
      console.log('Successfully uploaded file.', data);
      return data
    });
    //
  }
}