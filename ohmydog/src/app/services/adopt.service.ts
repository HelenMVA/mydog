import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AdoptService {
  apiURL = 'http://localhost:8000/back/';
  constructor(private http: HttpClient) { }

  getAdoptionDogs() {
    return this.http.get(`${this.apiURL}adoption`);
  }
  adopter(dog) {
    return this.http.post(`${this.apiURL}adoption/request`, dog);
  }
}
