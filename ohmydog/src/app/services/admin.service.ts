import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  apiURL = 'http://localhost:8000/back/';
  constructor(private http: HttpClient) { }
  getAllDemandes() {
    return this.http.get(`${this.apiURL}admin`);
  }

  //* ACCEPT /REJECT demand adoptation//

  accept(id) {
    return this.http.put(`${this.apiURL}admin/accept/${id}`, 'accept');
  }
  changeAvailableDog(id, dog_avalaible) {
    return this.http.put(`${this.apiURL}admin/availableDog/${id}`, dog_avalaible);
  }
  reject(id) {
    return this.http.put(`${this.apiURL}admin/reject/${id}`, 'reject');
  }
  sendUploadImg(annonce) {
    return this.http.post(`${this.apiURL}admin/annonce`, annonce);
  }
}
