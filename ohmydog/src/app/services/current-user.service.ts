import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../Models/user';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {
  /**************************
   * subscription currentUser
   *************************/
  private savedUser: User;
  public currentUser: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  constructor() { }

  /**************************
   * Quand un User  est connecte,
   * on l'enregistre dans le localeStorage et on l'envoie aux observers
   *************************/

  setCurrenUser(usr: User) {
    this.savedUser = usr;
    this.currentUser.next(usr);
  }
}
