import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DogsService {
  apiURL = 'http://localhost:8000/back/adoption/';
  constructor(private http: HttpClient) { }

  getDog(slug) {
    return this.http.get(`${this.apiURL}${slug}`);
  }
}
