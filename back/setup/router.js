import express from "express";
import usersRoutes from "../modules/user/routes";
import adoptionRoutes from "../modules/adoption/routes";
import adminRoutes from "../modules/admin/routes";
import keepRoutes from "../modules/keep/routes"

const Router = (server) => {
  //Project router
  server.use("/back/users", usersRoutes);
  server.use("/back/adoption", adoptionRoutes);
  server.use("/back/admin", adminRoutes);
  server.use("/back/keep", keepRoutes);
};

export default Router;
