import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import morgan from "morgan";
import cors from "cors"
import fileUpload from "express-fileupload"
import router from "./router";
// import databaseInitialisation from "./databaseInitialisation";

const serverInitialisation = (server) => {
    console.info('SETUP - Loading modules...')
    //console.log(process.env)
    // databaseInitialisation()
    server.use(bodyParser.json())
    server.use(bodyParser.urlencoded({ extended: false }))

    // Request body cookie parser
    server.use(cookieParser());
    server.use(cors());
    server.use(fileUpload());
    server.use(express.static(__dirname + '../public'));
    server.use(morgan("tiny"));
    // Initializing our routes
    router(server);
};

export default serverInitialisation;
