import AdminServices from "./service";
var path = require('path');
// var uploadPath = path.resolve(__dirname, '../../public/img/upload_img/')


const AdminController = {
  getAll: (req, res) => {
    AdminServices.getAll(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  acceptDemand: (req, res) => {
    AdminServices.acceptDemand(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  availableDog: (req, res) => {
    console.log(req);
    AdminServices.availableDog(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  rejectDemand: (req, res) => {
    AdminServices.rejectDemand(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  adoptannonce: (req, res) => {
    AdminServices.adoptannonce(req, (result) => {
      // console.log(result);
      result.success
        ? res.status(201).send(result)
        : res.status(404).send(result);
    });
  },
};

export default AdminController;
