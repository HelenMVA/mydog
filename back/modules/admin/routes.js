import express from "express";
import AdminController from "./controller";
const router = express.Router();

router.get("/", AdminController.getAll);
// router.post("/request", AdoptionController.adoptDog);
router.put("/accept/:id", AdminController.acceptDemand);
router.put("/reject/:id", AdminController.rejectDemand);
router.put("/availableDog/:id", AdminController.availableDog);
router.post("/annonce", AdminController.adoptannonce);


export default router;
