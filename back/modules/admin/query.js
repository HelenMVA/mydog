import db from "../../setup/database";

const Queries = {
  getAll: (param, successCallback, failureCallback) => {
    let sqlQuery =
      "SELECT * FROM users, user_has_adoption, dogs_adoption_list WHERE users.id = user_has_adoption.id_user AND user_has_adoption.id_dog = dogs_adoption_list.dog_id";

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No demandes.");
      }
    });
  },
  acceptDemand: (param, successCallback, failureCallback) => {
    let idDemand = param.params.id;
    let st = param.params.status;
    let sqlQuery = `UPDATE user_has_adoption
                     SET status="accept"
                     WHERE id_demand=${idDemand}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Demande acceté");
      }
    });
  },

  availableDog: (param, successCallback, failureCallback) => {
    let idDog = param.params.id;
    console.log(idDog);
    let sqlQuery = `UPDATE dogs_adoption_list
                     SET avalaible = 0
                     WHERE dog_id=${idDog}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("vous avez modifir avalaible dog ");
      }
    });
  },


  rejectDemand: (param, successCallback, failureCallback) => {
    let idDemand = param.params.id;
    console.log(param.params);
    let st = param.params.status;
    let sqlQuery = `UPDATE user_has_adoption
                     SET status="reject"
                     WHERE id_demand=${idDemand}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Demande rejeté");
      }
    });
  },

  adoptannonce: (imgName, successCallback, failureCallback) => {
    // const file = `../../public/${param}`;
    let sqlQuery = `INSERT INTO dogs_adoption_list (dog_id, photo) VALUES (NULL, "${imgName}")`;
    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("fichie enregistrer");
      }
    });
  },

};

export default Queries;
