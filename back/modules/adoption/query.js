import db from "../../setup/database";

const Queries = {
  getAll: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM dogs_adoption_list WHERE avalaible="1"`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No Dogs.");
      }
    });
  },
  getDog: (param, successCallback, failureCallback) => {
    console.log(param.params);
    let sqlQuery = `SELECT * FROM dogs_adoption_list WHERE slug="${param.params.slug}"`;
    console.log(sqlQuery);

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No dog with this id");
      }
    });
  },

  adoptDog: (param, successCallback, failureCallback) => {
    console.log(param.body);
    let sqlQuery = `INSERT INTO user_has_adoption (id_demand, id_dog, id_user, phone, reason, status) VALUES (NULL,"${param.body.id_dog}","${param.body.id_user}", "${param.body.phone}", "${param.body.reason}", 'pending')`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Adoption enregistrer");
      }
    });
  },
};

export default Queries;
