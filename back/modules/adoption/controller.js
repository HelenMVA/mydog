import AdoptionServices from "./service";
import UserServices from "../user/service";

const AdoptionController = {
  getAll: (req, res) => {
    AdoptionServices.getAll(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  getDog: (req, res) => {
    AdoptionServices.getDog(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  adoptDog: (req, res) => {
    console.dir(req);
    AdoptionServices.adoptDog(req, (result) => {
      console.log(result);
      result.success
        ? res.status(201).send(result)
        : res.status(404).send(result);
    });
  },
};

export default AdoptionController;
