import db from "../../setup/database";

// Notre query s'occupe d'effectuer la requête sur la base de donneés et de renvoyer au service les datas
const Queries = {
  // authenticate: (user, successCallback, failureCallback) => {
  //     let sqlQuery = `SELECT * FROM users WHERE name="${user.email}" AND password="${user.password}"`;
  //
  //     db.query(sqlQuery, (err, rows) => {
  //         if (err) {
  //             return failureCallback(err);
  //         }
  //         if (rows.length > 0) {
  //             return successCallback(rows[0]);
  //         } else {
  //             return successCallback("Incorrect username or password combinaison");
  //         }
  //     });
  // },
  getByUserEmail: (email) => {
    let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  getByUsername: (email) => {
    let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  getUserInformationsByUserId: (id) => {
    let sqlQuery = `SELECT *
    from users WHERE id = "${id}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  register: async (users) => {
    return new Promise((resolve, reject) => {
      let sqlQuery = `INSERT INTO users (id, firstname, lastname, email, password, user_role,rating, visibility, blocked) VALUES (NULL,"${users.firstname}","${users.lastname}", "${users.email}", "${users.hashedPassword}","user",0, 1, 0)`;

      db.query(sqlQuery, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });
  },
  getUserData: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM users WHERE id ="${param.params.id}"`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No data by this user");
      }
    });
  },
  getUserDog: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT user_has_adoption.id_demand,user_has_adoption.id_dog, user_has_adoption.id_user, user_has_adoption.phone, user_has_adoption.reason, user_has_adoption.status, dogs_adoption_list.dog_id, dogs_adoption_list.name, dogs_adoption_list.age,dogs_adoption_list.photo,dogs_adoption_list.description,dogs_adoption_list.sexe,dogs_adoption_list.race
    FROM dogs_adoption_list, user_has_adoption WHERE user_has_adoption.id_dog = dogs_adoption_list.dog_id
    AND user_has_adoption.id_user="${param.params.id}"`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No dog by this user");
      }
    });
  },

  getUserKeepDog: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM user_has_garde WHERE user_id="${param.params.id}"`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No keep dog by this user");
      }
    });
  },

  message: (param, successCallback, failureCallback) => {
    let sqlQuery = `INSERT INTO user_has_message (id_message, from_user_id, to_user_id, message) VALUES (NULL,"${param.body.from_user_id}","${param.body.to_user_id}", "${param.body.message}")`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Message send");
      }
    });
  },
  getUserMessages: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM user_has_message 
    WHERE user_has_message.from_user_id = "${param.params.fromid}" and user_has_message.to_user_id = "${param.params.id}"
    UNION
    SELECT * FROM user_has_message 
    WHERE user_has_message.from_user_id = "${param.params.id}" and user_has_message.to_user_id = "${param.params.fromid}"
    ORDER BY timestamp ASC`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No messages by this user");
      }
    });
  },
  getMyUsers: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT firstname, lastname, id FROM user_has_message, users 
    WHERE user_has_message.from_user_id = "${param.params.id}" and users.id = user_has_message.to_user_id
    UNION
    SELECT firstname, lastname, id FROM user_has_message, users 
    WHERE user_has_message.to_user_id = "${param.params.id}" and users.id = user_has_message.from_user_id`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("pas de users");
      }
    });
  },
  rejectMyAdoptDemand: (param, successCallback, failureCallback) => {
    let idDemand = param.params.id;
    // console.log(param.params);
    let st = param.params.status;
    let sqlQuery = `UPDATE user_has_adoption
                     SET status="user-cancel"
                     WHERE id_demand=${idDemand}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Demande rejeté");
      }
    });
  },
  getMyAnnonces: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM dogs_garde_list 
    WHERE owner_id = "${param.params.id}"`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("pas d'annonces");
      }
    });
  },

  deleteAvatar: (param, successCallback, failureCallback) => {
    console.dir(param.params.id);
    let idUser = param.params.id;
    let sqlQuery = `UPDATE users
                     SET avatar_url="null"
                     WHERE id=${idUser}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("avatar deleted");
      }
    });
  },

  editAvatar: (req, successCallback, failureCallback) => {
    let idUser = req.params.id;
    let url = req.body.avatar_url;
    console.log(url);
    let sqlQuery = `UPDATE users
                     SET avatar_url="${url}"
                     WHERE id=${idUser}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback(url);
      }
    });
  },
};

export default Queries;
