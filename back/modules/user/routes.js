import express from "express";
const router = express.Router();

import UserController from "./controller";

//Public routes
router.post("/authenticate", UserController.authenticate);
router.post("/register", UserController.register);

router.get("/:id", UserController.getUserDog);
router.get("/keepDog/:id", UserController.getUserKeepDog);

router.get("/me/:id", UserController.getUserData);

router.post("/message", UserController.message);
router.get("/messages/:id/:fromid", UserController.getUserMessages);
router.get("/myusers/:id/", UserController.getMyUsers);

router.put("/rejectMyAdoptDemand/:id", UserController.rejectMyAdoptDemand);

router.get("/myannonces/:id", UserController.getMyAnnonces);

router.put("/deleteAvatar/:id", UserController.deleteAvatar);
router.put("/editAvatar/:id", UserController.editAvatar);


export default router;
