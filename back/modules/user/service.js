import UserQueries from "./query";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../../config/server";

const UserServices = {
  authenticate: async (body) => {
    let { email, password } = body;

    if (typeof email !== "string" || typeof password !== "string") {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type",
        },
      };
    }

    const user = await UserQueries.getByUserEmail(email);

    if (!user) {
      return {
        status: 403,
        payload: { success: false, message: "Username not found" },
      };
    }

    const passwordMatched = await bcrypt.compare(password, user.password);

    if (passwordMatched) {
      const token = jwt.sign(
        { id: user.id, role: user.user_role },
        config.secret
      );

      const { password, ...userWithoutPassword } = user;
      return {
        status: 200,
        payload: {
          success: true,
          message: "User correctly authenticated",
          data: { token: token, user: userWithoutPassword },
        },
      };
    }

    return {
      status: 403,
      payload: { success: false, message: "Username & password missmatch" },
    };
  },
  register: async (body) => {
    console.log(body);
    let { firstname, lastname, email, password } = body;
    console.log(firstname);
    if (
      typeof firstname !== "string" ||
      typeof lastname !== "string" ||
      typeof email !== "string" ||
      typeof password !== "string"
    ) {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type",
        },
      };
    }
    const user = await UserQueries.getByUsername(email);

    if (user) {
      return {
        status: 403,
        payload: { success: false, message: "Username existe" },
      };
    }

    return bcrypt
      .genSalt()
      .then((salt) => bcrypt.hash(password, salt))
      .then((hashedPassword) =>
        UserQueries.register({ firstname, lastname, email, hashedPassword })
      )
      .then((user) => ({
        status: 201,
        payload: { success: true, message: "User successfully registered" },
      }))
      .catch((err) => ({
        status: 400,
        payload: { success: false, message: err },
      }));
  },
  getUserWithToken: async (req) => {
    const user = await UserQueries.getUserInformationsByUserId(req.user.id);

    if (!user) {
      return {
        status: 401,
        payload: { success: false, message: "User not found" },
      };
    }

    const { password, ...userWithoutPassword } = user;

    return {
      status: 200,
      payload: {
        success: true,
        message: "User correctly authenticated",
        data: { user: userWithoutPassword },
      },
    };
  },
  getUserData: (req, callback) => {
    UserQueries.getUserData(
      req,
      (response) => {
        return callback({
          success: true,
          message: "User information recuperé",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getUserDog: (req, callback) => {
    UserQueries.getUserDog(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Dogs retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getUserKeepDog: (req, callback) => {
    UserQueries.getUserKeepDog(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Demand keep dogs sont recuperes",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

  message: (req, callback) => {
    UserQueries.message(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getUserMessages: (req, callback) => {
    UserQueries.getUserMessages(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Messages retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getMyUsers: (req, callback) => {
    UserQueries.getMyUsers(
      req,
      (response) => {
        return callback({
          success: true,
          message: "ussers recuperés",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

  rejectMyAdoptDemand: (req, callback) => {
    UserQueries.rejectMyAdoptDemand(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getMyAnnonces: (req, callback) => {
    UserQueries.getMyAnnonces(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Annonces reuperés",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

  deleteAvatar: (req, callback) => {
    UserQueries.deleteAvatar(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  editAvatar: (req, callback) => {
    UserQueries.editAvatar(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

};

export default UserServices;
