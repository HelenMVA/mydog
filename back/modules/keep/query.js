import db from "../../setup/database";

const Queries = {

  addAnnonce: (param, successCallback, failureCallback) => {
    let sqlQuery = `INSERT INTO dogs_garde_list (dog_id ,owner_id, 	name, age, photo, characteristic , sexe , race, slug,classification, love_dog, love_cat, love_child, date_start, date_fin) 
    VALUES (NULL,"${param.body.owner_id}","${param.body.name}", "${param.body.age}", "${param.body.photo}","${param.body.characteristic}", "${param.body.sexe}","${param.body.race}","${param.body.dogName}","${param.body.classification}","${param.body.love_dog}","${param.body.love_cat}","${param.body.love_child}", "${param.body.date_start}", "${param.body.date_fin}")`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback(param.body);
      }
    });
  },
  getKeepDogs: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `dogs_garde_list`";

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No Dogs.");
      }
    });
  },

  keepDogDemand: (param, successCallback, failureCallback) => {
    console.log(param.body);
    let sqlQuery = `INSERT INTO user_has_garde (id_demand, dog_id ,user_id, owner_id) 
    VALUES (NULL,"${param.body.dog_id}", "${param.body.user_id}","${param.body.owner_id}")`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("demland etait bien enregistré");
      }
    });
  },
};

export default Queries;
