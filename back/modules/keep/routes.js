import express from "express";
const router = express.Router();

import KeepController from "./controller";
import authorize from "../../helpers/autorize";

//Public routes
// router.get("/", authorize(['user']), AdoptionController.getAll);
router.post("/annonce", KeepController.addAnnonce);
router.get("/dogs", KeepController.getKeepDogs);
router.post("/keepDog", KeepController.keepDogDemand);


export default router;
