import KeepQueries from "./query";

const KeepServices = {
  addAnnonce: (req, callback) => {
    KeepQueries.addAnnonce(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getKeepDogs: (req, callback) => {
    KeepQueries.getKeepDogs(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

  keepDogDemand: (req, callback) => {
    KeepQueries.keepDogDemand(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
};

export default KeepServices;
