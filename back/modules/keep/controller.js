import KeepServices from "./service";


const KeepController = {
  addAnnonce: (req, res) => {
    KeepServices.addAnnonce(req, (result) => {
      console.log(result);
      result.success
        ? res.status(201).send(result)
        : res.status(404).send(result);
    });
  },
  getKeepDogs: (req, res) => {
    KeepServices.getKeepDogs(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  keepDogDemand: (req, res) => {
    KeepServices.keepDogDemand(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
};

export default KeepController;
