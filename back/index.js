import express from 'express'
import connection from "./setup/database";


// App Imports
import serverInitialisation from './setup/serverInitialisation'
import startServer from "./setup/startServer"

// Create express server
const server = express();

// Setup server
serverInitialisation(server);

// Start server
startServer(server);
